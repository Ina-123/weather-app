import { Clouds } from "./clouds.model";
import { Main } from "./main.model";
import { Rain } from "./rain.models";
import { Sys } from "./sys.model";
import { Temp } from "./temp.model";
import { Weather } from "./weather.model";
import { Wind } from "./wind.model";

export class List{
    dt!: number
  sunrise!: number
  sunset!: number
  temp!: Temp;
  pressure!: number
  humidity!: number
  weather!: Weather[]
  speed!: number
  deg!: number
  gust!: number
  clouds!: number
  pop!: number
  rain?: number
}