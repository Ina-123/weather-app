import { City } from "./city.model"
import { List } from "./list.model"

export class ZipCodeForecast {
  city!: City
  cod!: string
  message!: number
  cnt!: number
  list!: List[];
  zipCode! : number;
}