export class Weather {
    id! : number;
    main! : string;
    description! : string;
    icon! : string;
    imgUrl? : string | null;
}