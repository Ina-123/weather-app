import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ZipCodeWeather } from '../models/zip-code-weather.models';
import { environment } from '../../../environments/environment';
import { ZipCodeForecast } from '../models/zip-code-forecast.model';

@Injectable()

export class WeatherService {

  constructor(private httpClient : HttpClient) { }

  getZipCodeWeather(zipCode : number) : Observable<ZipCodeWeather>{
    return this.httpClient.get<ZipCodeWeather>(`${environment.apiUrl}/weather?zip=${zipCode}&appid=${environment.api_Key}`);
  }

  getZipCodeForecast(zipCode : number) : Observable<ZipCodeForecast>{

    return this.httpClient.get<ZipCodeForecast>(`${environment.apiUrl}/forecast/daily?zip=${zipCode}&appid=${environment.api_Key}&cnt=5`);
  }
}
