export const WeatherIcons : string[] = [
    'rain',
    'clouds',
    'sun',
    'snow'
]