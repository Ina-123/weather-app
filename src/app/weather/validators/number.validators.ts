import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function validateNumber(): ValidatorFn {
    return (ctrl : AbstractControl) : null | ValidationErrors => {
        if(!isNaN(ctrl.value)){
            return null;
        }else{
            return {validateNumber : ctrl.value}
        }
        
    };
}