import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ZipCodeListComponent } from './components/zip-code-list/zip-code-list.component';
import { ZipCodeForecastComponent } from './components/zip-code-forecast/zip-code-forecast.component';

const routes: Routes = [
  {path : '', component : ZipCodeListComponent},
  {path : 'forecast/:zipcode', component : ZipCodeForecastComponent},
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherRoutingModule { }
