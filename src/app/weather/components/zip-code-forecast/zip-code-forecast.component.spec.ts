import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZipCodeForecastComponent } from './zip-code-forecast.component';

describe('ZipCodeForecastComponent', () => {
  let component: ZipCodeForecastComponent;
  let fixture: ComponentFixture<ZipCodeForecastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ZipCodeForecastComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ZipCodeForecastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
