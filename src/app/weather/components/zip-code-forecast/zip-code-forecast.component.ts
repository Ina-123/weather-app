import { Component, OnInit } from '@angular/core';
import { ZipCodeForecast } from '../../models/zip-code-forecast.model';
import { Observable, catchError, map, switchMap, tap, throwError } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { WeatherService } from '../../services/weather.service';
import { List } from '../../models/list.model';
import { Weather } from '../../models/weather.model';
import { WeatherIcons } from '../../constants/weather-icon.constants';

@Component({
  selector: 'app-zip-code-forecast',
  templateUrl: './zip-code-forecast.component.html',
  styleUrl: './zip-code-forecast.component.scss'
})
export class ZipCodeForecastComponent implements OnInit{
  zipCodeForecast$! : Observable<ZipCodeForecast>
  zipCode! : number;
  errorMessage!: string | null;

  constructor(private activateRoute : ActivatedRoute,
    private weatherService : WeatherService,
    private router : Router){}

  ngOnInit(): void {
    this.zipCodeForecast$ = this.activateRoute.params.pipe(
      tap(params => {this.zipCode = +params['zipcode']}),
      switchMap(() => this.weatherService.getZipCodeForecast(this.zipCode)),
      map((zipCodeFore : ZipCodeForecast) => ({...zipCodeFore, 
        zipCode : this.zipCode, 
        list : zipCodeFore.list.map((listData : List) => ({...listData,
          weather : listData.weather.map((weatherData : Weather) => ({...weatherData,
            imgUrl : WeatherIcons.includes(weatherData.main.toLowerCase())? `./assets/imgs/${weatherData.main.toLowerCase()}.png` : null}))}))})),
      catchError((error : string) =>{
        this.errorMessage = error;
        return throwError('error');
      })
    )
  }

  getDate(timestamp: number): Date {
    const date = new Date(timestamp * 1000);
    return date;
  }
  
  onGoBack() : void{
    this.router.navigateByUrl('/');
  }
}
