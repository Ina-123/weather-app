import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ZipCodeWeather } from '../../models/zip-code-weather.models';

@Component({
  selector: 'app-zip-code-list-item',
  templateUrl: './zip-code-list-item.component.html',
  styleUrl: './zip-code-list-item.component.scss'
})
export class ZipCodeListItemComponent {
  @Input() zipCodeWeather! : ZipCodeWeather;
  @Output() zipCodeWeatherDeleted = new EventEmitter<{zipCode : number}>();
  @Output() zipCodeWeatherForecast = new EventEmitter<{zipCode : number}>();
  onDeleteZipCode() : void{
    this.zipCodeWeatherDeleted.emit({zipCode : this.zipCodeWeather.zipCode});
  }

  onForecastZipCode(): void{
    this.zipCodeWeatherForecast.emit({zipCode : this.zipCodeWeather.zipCode})
  }
  
}
