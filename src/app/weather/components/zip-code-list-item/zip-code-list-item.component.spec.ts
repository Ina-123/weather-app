import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZipCodeListItemComponent } from './zip-code-list-item.component';

describe('ZipCodeListItemComponent', () => {
  let component: ZipCodeListItemComponent;
  let fixture: ComponentFixture<ZipCodeListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ZipCodeListItemComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ZipCodeListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
