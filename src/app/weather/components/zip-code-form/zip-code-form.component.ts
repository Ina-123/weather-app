import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { validateNumber } from '../../validators/number.validators';

@Component({
  selector: 'app-zip-code-form',
  templateUrl: './zip-code-form.component.html',
  styleUrl: './zip-code-form.component.scss'
})
export class ZipCodeFormComponent implements OnInit{
  mainForm! : FormGroup;
  zipCodeCtrl! : FormControl;
  @Output() zipCodeWeatherAdded = new EventEmitter<{zipCode : number}>(); 

  constructor(private formBuilder : FormBuilder){}

  ngOnInit(): void {
    this.initFromControls();
    this.initForm();
  }

  private initForm() : void{
    this.mainForm = this.formBuilder.group({
      zipCodeCtrl : this.zipCodeCtrl
    })
  }

  private initFromControls(): void{
    this.zipCodeCtrl = this.formBuilder.control('', [Validators.required, validateNumber()])
  }

  private resetForm(): void{
    this.mainForm.reset();
  }

  getFormControlErrorText(ctrl : AbstractControl){
    if(ctrl.hasError('required')){
      return 'Is required';
    }else if(ctrl.hasError('validateNumber')){
      return 'Must be a number!';
    }else{
      return '';
    }
  }

  onSubmitForm() : void{
    if(this.mainForm.invalid){
      return ;
    }
    let zipCode = this.zipCodeCtrl.value;
    this.resetForm();
    this.zipCodeWeatherAdded.emit({zipCode : zipCode});
  }

}
