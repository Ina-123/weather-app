import { Component, OnInit } from '@angular/core';
import { Observable, catchError, delay, map, of, take, tap, throwError } from 'rxjs';
import { ZipCodeWeather } from '../../models/zip-code-weather.models';
import { WeatherService } from '../../services/weather.service';
import { Weather } from '../../models/weather.model';
import { WeatherIcons } from '../../constants/weather-icon.constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-zip-code-list',
  templateUrl: './zip-code-list.component.html',
  styleUrl: './zip-code-list.component.scss'
})
export class ZipCodeListComponent implements OnInit{
  zipCodeWeather$! : Observable<ZipCodeWeather>;
  zipCodes! : ZipCodeWeather[];
  errorMessage! : string | null;

  constructor(
    private weatherService : WeatherService,
    private router : Router
  ){
    
    const storedZipCodes = localStorage.getItem('zipCodes');
    if(storedZipCodes){
      this.zipCodes = JSON.parse(storedZipCodes);
    }else{
      this.zipCodes = [];
    }
   
  }

  ngOnInit(): void {
 
  }


  onZipCodeWeatherAdded(data : {zipCode : number}) : void {
    if(!data?.zipCode){
      return;
    }
    this.errorMessage = null;
    const zipCode = data.zipCode;
    this.zipCodeWeather$ = this.weatherService.getZipCodeWeather(zipCode).pipe(
      delay(1000),
      take(1),
      map((data : ZipCodeWeather) => {
        let weatherConditions: Weather[] = data.weather.map((weather : Weather) => ({...weather, imgUrl : WeatherIcons.includes(weather.main.toLowerCase())? `./assets/imgs/${(weather.main).toLowerCase()}.png` : null}));
        return { ...data, zipCode : zipCode, weather: weatherConditions };
      }),
      tap((updatedData : ZipCodeWeather) => {
        
        const index = this.zipCodes.findIndex((zipCode : ZipCodeWeather) => zipCode.zipCode === updatedData.zipCode);
        
        if(index !== -1){
          this.zipCodes[index] = {...updatedData}
        }else{
          this.zipCodes.push({...updatedData});
        }
      
        localStorage.setItem('zipCodes',JSON.stringify(this.zipCodes));
      }),
      catchError((error : string) =>{
        this.errorMessage = error;
        return throwError('error');
      })
    );

    this.zipCodeWeather$.subscribe();
  }

  onDeleteZidCode(data : {zipCode : number}) : void{
    if(!data?.zipCode){
      return;
    }
    
    const zipeCodeWeather = data.zipCode;
    
    const index = this.zipCodes.findIndex((code : ZipCodeWeather) => code.zipCode === zipeCodeWeather);
 
    if(index !== -1){
      this.zipCodes.splice(index, 1);
      localStorage.setItem('zipCodes',JSON.stringify(this.zipCodes));
    }
  }

  onForecastZipCode(data : {zipCode : number}) : void{
    if(!data?.zipCode){
      return;
    }
    
    this.router.navigateByUrl(`forecast/${data.zipCode}`)
  }



}
