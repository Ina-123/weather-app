import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherRoutingModule } from './weather-routing.module';
import { ZipCodeListComponent } from './components/zip-code-list/zip-code-list.component';
import { ZipCodeFormComponent } from './components/zip-code-form/zip-code-form.component';
import { SharedModule } from '../shared/shared.module';
import { ZipCodeListItemComponent } from './components/zip-code-list-item/zip-code-list-item.component';
import { WeatherService } from './services/weather.service';
import { ZipCodeForecastComponent } from './components/zip-code-forecast/zip-code-forecast.component';


@NgModule({
  declarations: [
    ZipCodeListComponent,
    ZipCodeFormComponent,
    ZipCodeListItemComponent,
    ZipCodeForecastComponent
  ],
  imports: [
    CommonModule,
    WeatherRoutingModule,
    SharedModule
  ],
  providers : [
    WeatherService
  ]
})
export class WeatherModule { }
