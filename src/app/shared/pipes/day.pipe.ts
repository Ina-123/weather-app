import { Pipe, PipeTransform } from '@angular/core';
import { DaysOfWeek } from '../enums/days.enum';

@Pipe({
  name: 'day'
})
export class DayPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    const date = new Date(value * 1000);
    return DaysOfWeek[date.getDay()];
  }

}
