import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DayPipe } from './pipes/day.pipe';



@NgModule({
  declarations: [
    DayPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports : [
    ReactiveFormsModule,
    DayPipe
  ]
})
export class SharedModule { }
