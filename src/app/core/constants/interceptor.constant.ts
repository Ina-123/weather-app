import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorInterceptor } from "../interceptors/error.interceptor";

export const HttpInterceptorsProviders = [
    {
        provide : HTTP_INTERCEPTORS,
        useClass : ErrorInterceptor,
        multi : true
    }
]